#MVP приложение для расчёта зарплаты
____
## Первый запуск
![Image alt](https://github.com/reviakin-package/salnode/blob/main/app/src/main/res/raw/start.png)
____
## Главное меню
![Image alt](https://github.com/reviakin-package/salnode/blob/main/app/src/main/res/raw/main.png)
____
## Календарь
![Image alt](https://github.com/reviakin-package/salnode/blob/main/app/src/main/res/raw/calendar.png)
____
## Записанный день
![Image alt](https://github.com/reviakin-package/salnode/blob/main/app/src/main/res/raw/day.png)
____
## Общая статистика
![Image alt](https://github.com/reviakin-package/salnode/blob/main/app/src/main/res/raw/stat.png)
