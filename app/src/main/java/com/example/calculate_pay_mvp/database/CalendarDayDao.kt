package com.example.calculate_pay.database

import androidx.room.*
import io.reactivex.Flowable

@Dao
interface CalendarDayDao {

    @Query("SELECT * FROM calendar_entries")
    fun getAll() : Flowable<List<CalendarDay>>

    @Query("SELECT * FROM calendar_entries")
    fun getTimeAll(): List<CalendarDay>

    @Query("SELECT * FROM calendar_entries WHERE month = :month")
    fun getMonthAll(month: Int) : List<CalendarDay>

    @Query("SELECT * FROM calendar_entries WHERE year = :year")
    fun getYearAll(year: Int) : List<CalendarDay>

    @Insert
    fun insertDay(day : CalendarDay)
    
    @Update
    fun updateDay(day : CalendarDay)

    @Delete
    fun deleteDay(day : CalendarDay)

    @Query("SELECT EXISTS(SELECT date FROM calendar_entries WHERE date = :date)")
    fun checkExist(date: String) : Boolean

    @Query("SELECT * FROM calendar_entries WHERE date = :date")
    fun findEntry(date: String) : CalendarDay

    @Query("UPDATE calendar_entries SET salaryPerHour = :salary WHERE date = :date")
    fun updateSalaryPerHour(salary: Float, date: String)

    @Query("UPDATE calendar_entries SET countHour = :count WHERE date = :date")
    fun updateCountHour(count: Float, date: String)

    @Query("UPDATE calendar_entries SET timeTo = :timeTo WHERE date = :date")
    fun updateTimeTo(timeTo: String, date: String)

    @Query("UPDATE calendar_entries SET timeFrom = :timeFrom WHERE date = :date")
    fun updateTimeFrom(timeFrom: String, date: String)

    @Query("DELETE FROM calendar_entries WHERE date = :date")
    fun deleteDayDate(date: String): Int

    @Query("SELECT * FROM calendar_entries WHERE day >= :startPeriod and day <= :endPeriod and month = :month and year = :year")
    fun getPeriodStat(startPeriod:Int, endPeriod: Int, month: Int, year: Int): List<CalendarDay>

}