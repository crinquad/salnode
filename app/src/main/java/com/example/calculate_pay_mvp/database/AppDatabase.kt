package com.example.calculate_pay.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [CalendarDay::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun calendarDayDao(): CalendarDayDao
}