package com.example.calculate_pay_mvp.database

import android.util.Log
import com.example.calculate_pay.callable.DeleteOnDB
import com.example.calculate_pay.callable.FindEntryDB
import com.example.calculate_pay.callable.GetCurPeriodDB
import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.callable.*
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber

class DBHelper(private val database: AppDatabase) {

    fun updateSalaryPerHour(salaryPerHour: Float, date: String) : Single<CalendarDay>{
        return Single.fromCallable(UpdateSalaryDB(salaryPerHour, date, database))
    }

    fun updateTime(time: String, date: String, toOrFromCode:Int) : Single<CalendarDay>{
        return Single.fromCallable(UpdateTimeDB(time, date, toOrFromCode, database))
    }

    fun findEntryDB(date: String): Single<CalendarDay>{
        return Single.fromCallable(FindEntryDB(date, database))
    }

    fun getCurrentPeriod(days:List<Int>, months:List<Int>, years:List<Int>): Single<List<CalendarDay>>{
        return Single.fromCallable(GetCurPeriodDB(days, months, years, database))
    }

    fun deleteRow(date: String): Single<Int>{
        return Single.fromCallable(DeleteOnDB(date, database))
    }

    fun checkExist(day: OneDayDate): Single<Boolean> {
        return Single.fromCallable(CheckExistDB(day, database))
    }

    fun insertRow(day: CalendarDay) : Single<CalendarDay>{
        return Single.fromCallable(InsertInDB(day, database))
    }

    fun getRowsMonth(month: Int) : Single<List<CalendarDay>>{
        return Single.fromCallable(GetMonthAllDB(month, database))
    }

    fun getRowsYear(year: Int) : Single<List<CalendarDay>>{
        return Single.fromCallable(GetYearAllDB(year, database))
    }

    fun getAllRows() : Single<List<CalendarDay>>{
        return Single.fromCallable(GetTimeAllDB(database))
    }

}