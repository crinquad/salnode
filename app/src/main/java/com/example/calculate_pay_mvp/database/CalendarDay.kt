package com.example.calculate_pay.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.calculate_pay_mvp.utils.DateConvertUtils

@Entity(tableName = "calendar_entries")
data class CalendarDay(
    var date: String,
    var countHour: Float,
    var timeFrom: String,
    var timeTo: String,
    var salaryPerHour: Float
){

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    var salary: Float = countHour * salaryPerHour

    var day: Int = DateConvertUtils.getDayFromDate(date)

    var month: Int = DateConvertUtils.getMonthFromDate(date)

    var year: Int = DateConvertUtils.getYearFromDate(date)
}