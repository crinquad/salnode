package com.example.calculate_pay_mvp.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.calculate_pay_mvp.R
import com.example.calculate_pay_mvp.adapter.StatisticPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class StatisticAllFragment : Fragment(), TabLayoutMediator.TabConfigurationStrategy {

    companion object {
        val PAGE_COUNT = 3
    }

    lateinit var pager: ViewPager2
    lateinit var pagerAdapter: StatisticPagerAdapter
    lateinit var tabLayout: TabLayout
    lateinit var context: FragmentActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.context = context as FragmentActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.stat_all_fragment, null) as LinearLayout
        pager = view.findViewById(R.id.pager)
        pagerAdapter = StatisticPagerAdapter(context)
        pager.adapter = pagerAdapter
        tabLayout = view.findViewById(R.id.tabs)

        var tabsMediator = TabLayoutMediator(tabLayout, pager, this)
        tabsMediator.attach()

        return view
    }

    override fun onConfigureTab(tab: TabLayout.Tab, position: Int) {
        var inflater = LayoutInflater.from(context)
        var text: TextView = inflater.inflate(R.layout.custom_tab, null) as TextView
        when (position) {
            0 -> {
                text.setText(resources.getString(R.string.all_time))
                tab.setCustomView(text)
            }
            1 -> {
                text.setText(resources.getString(R.string.month))
                tab.setCustomView(text)
            }
            2 -> {
                text.setText(resources.getString(R.string.year))
                tab.setCustomView(text)
            }
        }
    }
}