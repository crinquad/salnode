package com.example.calculate_pay_mvp.fragment

import android.app.TimePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay.unit.WorkHours
import com.example.calculate_pay_mvp.R
import com.example.calculate_pay_mvp.utils.DateConvertUtils
import java.lang.ClassCastException
import java.util.*

class DayDialogFragment(var day: OneDayDate) : DialogFragment(), View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    lateinit var listener: OnAcceptClickListener
    private var mDateAndTime: Calendar = Calendar.getInstance()
    private var mIsLeftTime: Boolean = true
    private var mViewEx : View? = null
    private lateinit var mWorkHours: WorkHours
    private lateinit var mListFrom: List<Int>
    private lateinit var mListTo: List<Int>
    private lateinit var mEditSalaryHour: EditText
    private var salary: Float = 0F

    interface OnAcceptClickListener{
        fun onAcceptClick(time:Float, salary:Float, day: OneDayDate, timeFrom: String, timeTo:String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
            listener = context as OnAcceptClickListener
        }catch (e:ClassCastException){
            //...
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = inflater.inflate(R.layout.dialog_add_day, null)
        view.findViewById<TextView>(R.id.dateDialog).setText(DateConvertUtils.convertDateToShow(day))
        view.findViewById<Button>(R.id.btnCancel).setOnClickListener(this)
        view.findViewById<Button>(R.id.btnAccept).setOnClickListener(this)
        view.findViewById<TextView>(R.id.hoursFrom).setOnClickListener(this)
        view.findViewById<TextView>(R.id.hoursTo).setOnClickListener(this)

        mListFrom = view.findViewById<TextView>(R.id.hoursFrom).text.toString().split(":").map { it.trim().toInt() }
        mListTo = view.findViewById<TextView>(R.id.hoursTo).text.toString().split(":").map { it.trim().toInt() }
        mEditSalaryHour = view.findViewById(R.id.editSalaryHour)

        mWorkHours = WorkHours(
            mListFrom.get(0),
            mListFrom.get(1),
            mListTo.get(0),
            mListTo.get(1)
        )

        mViewEx = view

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnAccept -> {
                salary = if(mEditSalaryHour.text.toString() != "") mEditSalaryHour.text.toString().toFloat() else -100F
                if(salary != -100F) {
                    listener.onAcceptClick(
                        mWorkHours.calculateTime(),
                        salary,
                        day,
                        DateConvertUtils.convertTimeToShow(
                            mWorkHours.getHourFrom(),
                            mWorkHours.getMinuteFrom()
                        ),
                        DateConvertUtils.convertTimeToShow(
                            mWorkHours.getHourTo(),
                            mWorkHours.getMinuteTo()
                        )
                    )
                    dismiss()
                }else{
                    //...
                }
            }
            R.id.btnCancel -> {
                dismiss()
            }
            R.id.hoursFrom -> {
                mIsLeftTime = true
                setTime()
            }
            R.id.hoursTo -> {
                mIsLeftTime = false
                setTime()
            }
        }
    }

    private fun setTime(){
        TimePickerDialog(
            requireContext(),
            this,
            mDateAndTime.get(Calendar.HOUR_OF_DAY),
            mDateAndTime.get(Calendar.MINUTE),
            true).show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        var timeLeft = mViewEx?.findViewById<TextView>(R.id.hoursFrom)
        var timeRight = mViewEx?.findViewById<TextView>(R.id.hoursTo)
        if(mIsLeftTime){
            timeLeft?.setText(DateConvertUtils.convertTimeToShow(hourOfDay, minute))
        }else{
            timeRight?.setText(DateConvertUtils.convertTimeToShow(hourOfDay, minute))
        }
        mListFrom = timeLeft?.text.toString().split(":").map { it.trim().toInt() }
        mListTo = timeRight?.text.toString().split(":").map { it.trim().toInt() }

        mWorkHours = WorkHours(
            mListFrom.get(0),
            mListFrom.get(1),
            mListTo.get(0),
            mListTo.get(1)
        )
    }
}