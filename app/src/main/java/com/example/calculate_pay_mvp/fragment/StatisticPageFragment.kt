package com.example.calculate_pay.fragments

import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.calculate_pay_mvp.app.App
import com.example.calculate_pay_mvp.R
import com.example.calculate_pay_mvp.component.AppComponent
import com.example.calculate_pay_mvp.contracts.StatisticPageContract
import com.example.calculate_pay_mvp.presenter.StatisticPagePresenter
import com.example.calculate_pay_mvp.utils.StringSwithcUtils

class StatisticPageFragment : Fragment(), StatisticPageContract.View {

    private var mPageNumber: Int = 0
    lateinit var textHourCount: TextView
    lateinit var textDaysCount: TextView
    lateinit var textSalaryGlobal: TextView
    lateinit var appComponent: AppComponent
    lateinit var presenter: StatisticPagePresenter

    companion object{

        val PAGE_ALLTIME = 0
        val PAGE_MONTH = 1
        val PAGE_YEAR = 2

        val ARGUMENT_PAGE_NUMBER = "arg_page_number"

        var curPage: Int = 0

        fun newInstance(page: Int): StatisticPageFragment{
            var pageFragment = StatisticPageFragment()
            var arguments = Bundle()
            arguments.putInt(ARGUMENT_PAGE_NUMBER, page)
            pageFragment.arguments = arguments
            curPage = page
            return pageFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPageNumber = arguments?.getInt(ARGUMENT_PAGE_NUMBER) ?: 0
        appComponent = (requireActivity().application as App).appComponent
        presenter = appComponent.getStatisticPageComponent().getStatisticPagePresenter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view: View = layoutInflater.inflate(R.layout.pager_fragment, null)

        init(view)

        return view
    }

    override fun initPage(){
        when(curPage){
            PAGE_ALLTIME -> {
                presenter.initAllTime()
            }
            PAGE_MONTH -> {
                presenter.initMonth()
            }
            PAGE_YEAR -> {
               presenter.initYear()
            }
        }
    }

    override fun showDay(hours: Float, salary: Float, days: Int) {
        textHourCount.setText("${String.format("%.1f", hours)} ${resources.getString(R.string.hours)}")
        textSalaryGlobal.setText(String.format("%.1f", salary))
        textDaysCount.setText("${days} ${StringSwithcUtils.chooseDayString(days % 10)}")
    }

    fun init(view: View){
        textHourCount = view.findViewById(R.id.hoursCount)
        textDaysCount = view.findViewById(R.id.daysCount)
        textSalaryGlobal = view.findViewById(R.id.salaryGlobal)

        presenter.attachView(this)
        presenter.viewIsReady()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

}