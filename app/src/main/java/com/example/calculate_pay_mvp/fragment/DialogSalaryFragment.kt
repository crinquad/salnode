package com.example.calculate_pay_mvp.fragment

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.example.calculate_pay_mvp.R
import java.lang.ClassCastException

class DialogSalaryFragment : DialogFragment(), View.OnClickListener {

    lateinit var editSalary: EditText
    lateinit var listener: OnAcceptClickListener

    interface OnAcceptClickListener{
        fun onAcceptClick(newSalary: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try{
            listener = context as OnAcceptClickListener
        }catch (e: ClassCastException){
            //...
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = layoutInflater.inflate(R.layout.dialog_change_salaryperhour, null)
        editSalary = view.findViewById(R.id.editSalaryHour)
        view.findViewById<Button>(R.id.btnAccept).setOnClickListener(this)
        view.findViewById<Button>(R.id.btnCancel).setOnClickListener(this)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onClick(v: View) {
        when(v.id){
            R.id.btnCancel -> {
                dismiss()
            }
            R.id.btnAccept -> {
                if(!editSalary.text.toString().equals("")) {
                    listener.onAcceptClick(editSalary.text.toString())
                    dismiss()
                }
            }
        }
    }
}