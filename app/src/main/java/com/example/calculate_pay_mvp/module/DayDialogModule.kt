package com.example.calculate_pay_mvp.module

import androidx.fragment.app.DialogFragment
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.fragment.DayDialogFragment
import dagger.Module
import dagger.Provides

@Module
class DayDialogModule(var day: OneDayDate) {
    @Provides
    fun provideDayDialogFragment() : DayDialogFragment{
        return DayDialogFragment(day)
    }
}