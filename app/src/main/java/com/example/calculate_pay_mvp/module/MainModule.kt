package com.example.calculate_pay_mvp.module

import com.example.calculate_pay_mvp.app.AppPreferences
import com.example.calculate_pay_mvp.presenter.MainActivityPresenter
import com.example.calculate_pay_mvp.database.DBHelper
import com.example.calculate_pay_mvp.utils.InitFormatDayUtils
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class MainModule {
    @Provides
    fun provideMainActivityPresenter(initFormatDayUtils: InitFormatDayUtils, dbHelper: DBHelper): MainActivityPresenter {
        return MainActivityPresenter(
            initFormatDayUtils,
            dbHelper
        )
    }

    @Provides
    fun provideInitFormatDayUtils(preferences: AppPreferences, calendar: Calendar): InitFormatDayUtils{
        return InitFormatDayUtils(preferences, calendar)
    }
}