package com.example.calculate_pay_mvp.module

import com.example.calculate_pay_mvp.fragment.DialogSalaryFragment
import dagger.Module
import dagger.Provides

@Module
class DialogSalaryModule {
    @Provides
    fun provideSalaryDialog() : DialogSalaryFragment{
        return DialogSalaryFragment()
    }
}