package com.example.calculate_pay_mvp.module

import android.content.Context
import androidx.room.Room
import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay_mvp.app.AppScope
import com.example.calculate_pay_mvp.database.DBHelper
import dagger.Module
import dagger.Provides

@Module
class StorageModule(private val appDatabase: Class<AppDatabase>) {
    @Provides
    fun provideDBHelper(appDatabase: AppDatabase): DBHelper {
        return DBHelper(appDatabase)
    }

    @AppScope
    @Provides
    fun provideAppDatabase(context: Context): AppDatabase{
        return Room.databaseBuilder(
            context,
            appDatabase,
            "database"
        ).build()
    }
}