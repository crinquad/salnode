package com.example.calculate_pay_mvp.module

import com.example.calculate_pay.datemanagers.DateManager
import com.example.calculate_pay.datemanagers.RealDateManager
import com.example.calculate_pay_mvp.database.DBHelper
import com.example.calculate_pay_mvp.presenter.CalendarActivityPresenter
import dagger.Module
import dagger.Provides

@Module
class CalendarModule {
    @Provides
    fun provideCalendarActivityPresenter(dateManager: DateManager, realDateManager: RealDateManager, dbHelper:DBHelper) : CalendarActivityPresenter{
        return CalendarActivityPresenter(dateManager, realDateManager, dbHelper)
    }
}