package com.example.calculate_pay_mvp.module

import com.example.calculate_pay.datemanagers.RealDateManager
import com.example.calculate_pay_mvp.database.DBHelper
import com.example.calculate_pay_mvp.presenter.StatisticPagePresenter
import dagger.Module
import dagger.Provides

@Module
class StatisticPageModule {
    @Provides
    fun provideStatisticPagePresenter(dbHelper:DBHelper, realDateManager: RealDateManager) : StatisticPagePresenter{
        return StatisticPagePresenter(dbHelper, realDateManager)
    }
}