package com.example.calculate_pay_mvp.module

import com.example.calculate_pay_mvp.app.AppPreferences
import com.example.calculate_pay_mvp.presenter.FirstStartActivityPresenter
import dagger.Module
import dagger.Provides

@Module
class FirstStartModule {
    @Provides
    fun provideFirstStartActivityPresenter(preferences: AppPreferences) : FirstStartActivityPresenter {
        return FirstStartActivityPresenter(
            preferences
        )
    }
}