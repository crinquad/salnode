package com.example.calculate_pay_mvp.module

import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.database.DBHelper
import com.example.calculate_pay_mvp.presenter.ExistDayActivityPresenter
import dagger.Module
import dagger.Provides

@Module
class ExistDayModule(val day:OneDayDate) {
    @Provides
    fun provideExistDayActivityPresenter(dbHelper: DBHelper) : ExistDayActivityPresenter{
        return ExistDayActivityPresenter(dbHelper, day)
    }
}