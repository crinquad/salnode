package com.example.calculate_pay_mvp.module

import android.content.Context
import com.example.calculate_pay.adapter.CalendarAdapter
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.database.DBHelper
import dagger.Module
import dagger.Provides

@Module
class CalendarAdapterModule(var context: Context, var array: Array<OneDayDate>) {
    @Provides
    fun provideCalendarAdapter(dbHelper : DBHelper): CalendarAdapter{
        return CalendarAdapter(context, array, dbHelper)
    }
}