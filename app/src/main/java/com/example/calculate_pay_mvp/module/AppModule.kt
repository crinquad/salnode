package com.example.calculate_pay_mvp.module

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import com.example.calculate_pay_mvp.app.AppPreferences
import com.example.calculate_pay_mvp.app.AppScope
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class AppModule {

    @Provides
    fun provideBundle() : Bundle{
        return Bundle()
    }

    @Provides
    fun provideCalendar(): Calendar{
        return Calendar.getInstance()
    }

    @AppScope
    @Provides
    fun providePreferencesObject(preferences: SharedPreferences): AppPreferences {
        return AppPreferences(preferences)
    }

    @AppScope
    @Provides
    fun providePreferences(context: Context): SharedPreferences{
        return context.getSharedPreferences("prefs", MODE_PRIVATE)
    }
}