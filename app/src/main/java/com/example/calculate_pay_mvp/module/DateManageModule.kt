package com.example.calculate_pay_mvp.module

import com.example.calculate_pay.datemanagers.DateManager
import com.example.calculate_pay.datemanagers.RealDateManager
import dagger.Module
import dagger.Provides

@Module
class DateManageModule {
    @Provides
    fun provideDateManager(): DateManager{
        return DateManager()
    }

    @Provides
    fun provideRealDateManager(): RealDateManager{
        return RealDateManager()
    }
}