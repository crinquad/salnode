package com.example.calculate_pay_mvp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.GridView
import android.widget.TextView
import com.example.calculate_pay.adapter.CalendarAdapter
import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.datemanagers.RealDateManager
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.app.App
import com.example.calculate_pay_mvp.app.Arrays
import com.example.calculate_pay_mvp.R
import com.example.calculate_pay_mvp.component.AppComponent
import com.example.calculate_pay_mvp.component.CalendarComponent
import com.example.calculate_pay_mvp.contracts.CalendarContract
import com.example.calculate_pay_mvp.fragment.DayDialogFragment
import com.example.calculate_pay_mvp.module.CalendarAdapterModule
import com.example.calculate_pay_mvp.module.DayDialogModule
import com.example.calculate_pay_mvp.presenter.CalendarActivityPresenter

class CalendarActivity : AppCompatActivity(), CalendarContract.View, CalendarAdapter.OnCalendarItemListener, View.OnClickListener, DayDialogFragment.OnAcceptClickListener{

    private lateinit var mAdapter: CalendarAdapter
    private lateinit var mCalendar: GridView
    private lateinit var mTextDate: TextView
    private lateinit var mBtnLeft: Button
    private lateinit var mBtnRight: Button
    lateinit var dayDialog: DayDialogFragment

    lateinit var appComponent: AppComponent
    lateinit var calendarComponent: CalendarComponent
    lateinit var calendarActivityPresenter: CalendarActivityPresenter

    companion object{
        val DIALOG_TAG = "dialog"
        val SEND_DATA = "senddata"
    }

    override fun onResume() {
        super.onResume()
        calendarActivityPresenter.viewIsReady()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)
        init()
        calendarActivityPresenter.attachView(this)
    }

    fun init(){
        mCalendar = findViewById(R.id.calendarDays)
        mTextDate = findViewById(R.id.dateTime)
        mBtnLeft = findViewById(R.id.btnDateLeft)
        mBtnRight = findViewById(R.id.btnDateRight)

        mBtnLeft.setOnClickListener(this)
        mBtnRight.setOnClickListener(this)

        appComponent = (application as App).appComponent
        calendarComponent = appComponent.getCalendarComponent()
        calendarActivityPresenter = calendarComponent.getCalendarActivityPresenter()
    }

    override fun initAdapter(date: Array<OneDayDate>) {
        mAdapter = appComponent.getAdapterComponent(CalendarAdapterModule(this, date)).getCalendarAdapter()
        mCalendar.adapter = mAdapter
    }

    override fun showTextDate(month: Int, year: Int) {
        mTextDate.setText("${resources.getString(Arrays.month[month])} , $year")
    }

    override fun showDialog(day: OneDayDate) {
        dayDialog = appComponent.getDayDialogComponent(DayDialogModule(day)).getDayDialog()
        dayDialog.show(supportFragmentManager, DIALOG_TAG)
    }

    override fun showExistDay(day: OneDayDate) {
        var intent = Intent(this@CalendarActivity, ExistDayActivity::class.java)
        intent.putExtra(SEND_DATA, day)
        startActivity(intent)
    }

    override fun onItemClick(day: OneDayDate) {
        calendarActivityPresenter.showDay(day)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDateLeft -> {
                calendarActivityPresenter.refreshDate(RealDateManager.MONTH_DECREMENT)
            }
            R.id.btnDateRight -> {
                calendarActivityPresenter.refreshDate(RealDateManager.MONTH_INCREMENT)
            }
        }
    }

    override fun onAcceptClick(
        time: Float,
        salary: Float,
        day: OneDayDate,
        timeFrom: String,
        timeTo: String
    ) {
        var date = "${day.day}.${day.month}.${day.year}"
        calendarActivityPresenter.saveDay(CalendarDay(date, time, timeFrom, timeTo, salary))
    }

    override fun onDestroy() {
        super.onDestroy()
        calendarActivityPresenter.detachView()
    }
}