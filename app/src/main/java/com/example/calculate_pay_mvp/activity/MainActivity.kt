package com.example.calculate_pay_mvp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.calculate_pay_mvp.app.App
import com.example.calculate_pay_mvp.presenter.MainActivityPresenter
import com.example.calculate_pay_mvp.R
import com.example.calculate_pay_mvp.contracts.MainContract
import com.example.calculate_pay_mvp.utils.StringSwithcUtils

class MainActivity : AppCompatActivity(), MainContract.View, View.OnClickListener {

    lateinit var textDateOut: TextView
    lateinit var textDaysCount: TextView
    lateinit var textHourCount: TextView
    lateinit var textSalaryGlobal: TextView
    lateinit var btnCalendar: Button
    lateinit var btnStatistic: Button

    lateinit var presenter: MainActivityPresenter

    override fun onResume() {
        super.onResume()
        presenter.viewIsReady()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

        presenter = (application as App).appComponent.getMainComponent().getMainActivityPresenter()
        presenter.attachView(this)
    }

    fun init(){
        textDateOut = findViewById(R.id.textDateOut)
        textDaysCount = findViewById(R.id.textDaysCount)
        textHourCount = findViewById(R.id.textHoursCount)
        textSalaryGlobal = findViewById(R.id.textSalaryGlobal)
        btnCalendar = findViewById(R.id.btnCalendar)
        btnStatistic = findViewById(R.id.btnStatistic)

        btnCalendar.setOnClickListener(this)
        btnStatistic.setOnClickListener(this)
    }

    override fun showDaysOut(daysOut: Int) {
        textDateOut.setText("${resources.getString(R.string.before_pay)} $daysOut ${StringSwithcUtils.chooseDayString(daysOut)}")
    }

    override fun showDaysCount(daysCount: Int) {
        textDaysCount.setText("$daysCount ${StringSwithcUtils.chooseDayString(daysCount)}")
    }

    override fun showHourCount(hourCount: Float) {
        textHourCount.setText("${String.format("%.1f", hourCount)} ${resources.getString(
            R.string.hours
        )}")
    }

    override fun showSalaryGlobal(salaryGlobal: Float) {
        textSalaryGlobal.setText(String.format("%.1f", salaryGlobal))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnCalendar -> {
                startActivity(Intent(this@MainActivity, CalendarActivity::class.java))
            }
            R.id.btnStatistic -> {
                startActivity(Intent(this@MainActivity, StatisticActivity::class.java))
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}