package com.example.calculate_pay_mvp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.calculate_pay.adapter.SpinnerAdapter
import com.example.calculate_pay_mvp.app.App
import com.example.calculate_pay_mvp.presenter.FirstStartActivityPresenter
import com.example.calculate_pay_mvp.R
import com.example.calculate_pay_mvp.contracts.FirstStartContract

class FirstStartActivity : AppCompatActivity(), View.OnClickListener, AdapterView.OnItemSelectedListener, FirstStartContract.View {

    lateinit var btnAcceptOne: Button
    lateinit var btnAcceptTwo: Button
    lateinit var editDayOne: EditText
    lateinit var editDayTwo: EditText
    lateinit var editDay: EditText
    lateinit var spinnerGr: Spinner
    lateinit var formOne: LinearLayout
    lateinit var formTwo: LinearLayout
    lateinit var arrayGr: Array<String>
    lateinit var presenter: FirstStartActivityPresenter

    companion object {
        val ID_ONE_DAY = 0
        val ID_TWO_DAY = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_start)

        init()
        initAdapter()

        presenter = (application as App).appComponent.getFirstStartComponent().getFirstStartActivityPresenter()
        presenter.attachView(this)
        presenter.viewIsReady()
    }

    fun init(){
        arrayGr = arrayOf(
            resources.getString(R.string.onepayday),
            resources.getString(R.string.twopayday)
        )

        formOne = findViewById(R.id.formOnePay)
        formTwo = findViewById(R.id.formTwoPay)
        btnAcceptOne = findViewById(R.id.btnAcceptFormOne)
        btnAcceptTwo = findViewById(R.id.btnAcceptFormTwo)
        editDayOne = findViewById(R.id.editFirstDay)
        editDayTwo = findViewById(R.id.editSecondDay)
        editDay = findViewById(R.id.editDay)
        spinnerGr = findViewById(R.id.spinnerGr)

        btnAcceptTwo.setOnClickListener(this)
        btnAcceptOne.setOnClickListener(this)
    }

    fun initAdapter() {
        spinnerGr.adapter = SpinnerAdapter(this,
            R.layout.spinner_layout, arrayGr)
        spinnerGr.onItemSelectedListener = this@FirstStartActivity
    }

    override fun onClick(v: View?) {
        if(presenter.putReg()){
            startActivity(Intent(this@FirstStartActivity, MainActivity::class.java))
            finish()
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (position) {
            ID_ONE_DAY -> {
                formOne.visibility = View.VISIBLE
                formTwo.visibility = View.GONE
            }
            ID_TWO_DAY -> {
                formOne.visibility = View.GONE
                formTwo.visibility = View.VISIBLE
            }
        }
    }

    override fun getInputFormOne(): String {
        return editDay.text.toString()
    }

    override fun getInputFormTwo(): Array<String> {
        return arrayOf(editDayOne.text.toString(), editDayTwo.text.toString())
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        //...
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}