package com.example.calculate_pay_mvp.activity

import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.TimePicker
import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.app.App
import com.example.calculate_pay_mvp.R
import com.example.calculate_pay_mvp.component.AppComponent
import com.example.calculate_pay_mvp.contracts.ExistDayContract
import com.example.calculate_pay_mvp.fragment.DialogSalaryFragment
import com.example.calculate_pay_mvp.module.ExistDayModule
import com.example.calculate_pay_mvp.presenter.ExistDayActivityPresenter
import com.example.calculate_pay_mvp.utils.DateConvertUtils
import dagger.Lazy
import java.util.*

class ExistDayActivity : AppCompatActivity(),
    ExistDayContract.View,
    TimePickerDialog.OnTimeSetListener,
    View.OnClickListener,
    DialogSalaryFragment.OnAcceptClickListener {

    lateinit var textDate: TextView
    lateinit var textHoursFrom: TextView
    lateinit var textHoursTo: TextView
    lateinit var textPayHour: TextView
    lateinit var textSalary: TextView
    lateinit var btnBack: Button
    lateinit var btnDelete: Button

    lateinit var appComponent: AppComponent
    lateinit var presenter: ExistDayActivityPresenter
    lateinit var salaryDialog: Lazy<DialogSalaryFragment>

    companion object{
        val DIALOG_SALARY_TAG = "dlg_perHour"
        val MENU_ID_SALARY = 0
        val MENU_ID_TIMEFROM = 1
        val MENU_ID_TIMETO = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exist_day)
        init()

        appComponent = (application as App).appComponent
        salaryDialog = appComponent.getDialogSalaryComponent().getSalaryDialog()
        presenter = appComponent
            .getExistDayComponent(ExistDayModule(intent.extras?.getSerializable(CalendarActivity.SEND_DATA) as OneDayDate))
            .getExistDayActivityPresenter()
        presenter.attachView(this)
        presenter.viewIsReady()
    }

    fun init(){
        textDate = findViewById(R.id.textDate)
        textHoursFrom = findViewById(R.id.textHoursFrom)
        textHoursTo = findViewById(R.id.textHoursTO)
        textPayHour = findViewById(R.id.textHourPay)
        textSalary = findViewById(R.id.textSalary)
        btnBack = findViewById(R.id.btnBack)
        btnDelete = findViewById(R.id.btnDelete)

        btnBack.setOnClickListener(this)
        btnDelete.setOnClickListener(this)

        registerForContextMenu(textPayHour)
        registerForContextMenu(textHoursFrom)
        registerForContextMenu(textHoursTo)
    }

    override fun initTextFromEntry(day: CalendarDay) {
        textDate.setText(DateConvertUtils.convertDateToShow(day))
        textHoursFrom.setText("${day.timeFrom}")
        textHoursTo.setText("${day.timeTo}")
        textPayHour.setText(day.salaryPerHour.toString())
        textSalary.setText("${day.salary}")
    }

    override fun showTimeDialog() {
        var calendar = appComponent.getCalendar()
        TimePickerDialog(
            this@ExistDayActivity,
            this,
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            true
        ).show()
    }

    override fun showSalaryDialog() {
        salaryDialog.get().show(supportFragmentManager, DIALOG_SALARY_TAG)
    }

    override fun getDateText(): String {
        return textDate.text.toString()
    }

    override fun destroy() {
        finish()
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        when(v?.id){
            R.id.textHourPay -> {
                menu?.add(0,
                    MENU_ID_SALARY, 0, "Изменить")
            }
            R.id.textHoursFrom -> {
                menu?.add(0,
                    MENU_ID_TIMEFROM, 0, "Изменить")
            }
            R.id.textHoursTO -> {
                menu?.add(0,
                    MENU_ID_TIMETO, 0, "Изменить")
            }
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            MENU_ID_SALARY -> {
                presenter.changeSalaryHour()
            }
            MENU_ID_TIMEFROM -> {
                presenter.changeTimeFrom()
            }
            MENU_ID_TIMETO -> {
                presenter.changeTimeTo()
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        var time =
            DateConvertUtils.convertTimeToShow(
                hourOfDay,
                minute
            )
        presenter.updateTime(time)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnBack -> {
                onBackPressed()
                finish()
            }
            R.id.btnDelete -> {
                presenter.deleteDay()
            }
        }
    }

    override fun onAcceptClick(newSalary: String) {
        presenter.updateSalaryHour(newSalary.toFloat())
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }
}