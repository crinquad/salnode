package com.example.calculate_pay_mvp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.calculate_pay_mvp.app.App
import com.example.calculate_pay_mvp.app.AppPreferences
import com.example.calculate_pay_mvp.R

class StartActivity : AppCompatActivity() {

    lateinit var preferences: AppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        preferences = (application as App).appComponent.getPreferences()

        var firstStart = preferences.checkPref(AppPreferences.PREF_FIRST_CHECK)
        when(firstStart){
            AppPreferences.FIRST_CHECK_FALSE -> {
                startActivity(Intent(this@StartActivity, MainActivity::class.java))
                finish()
            }
            AppPreferences.FIRST_CHECK_TRUE -> {
                startActivity(Intent(this@StartActivity, FirstStartActivity::class.java))
                finish()
            }
        }
    }
}