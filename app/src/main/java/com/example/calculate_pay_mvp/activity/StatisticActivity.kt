package com.example.calculate_pay_mvp.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.calculate_pay_mvp.R
import com.example.calculate_pay_mvp.fragment.StatisticAllFragment

class StatisticActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var btnBack: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistic)

        btnBack = findViewById(R.id.btnBack)
        btnBack.setOnClickListener(this)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.statFragment, StatisticAllFragment())
            .commit()
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnBack -> {
                onBackPressed()
            }
        }
    }
}