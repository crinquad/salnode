package com.example.calculate_pay_mvp.utils

import com.example.calculate_pay_mvp.app.App.Companion.applicationContext
import com.example.calculate_pay_mvp.R

class StringSwithcUtils {

    companion object {
        fun chooseDayString(endDay: Int): String {
            when (endDay) {
                1 -> {
                    return applicationContext().resources.getString(R.string.day)
                }
                2, 3, 4 -> {
                    return applicationContext().resources.getString(R.string.dayss)
                }
            }
            return return applicationContext().resources.getString(R.string.days)
        }
    }
}