package com.example.calculate_pay_mvp.utils

import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.unit.OneDayDate

class DateConvertUtils {

    companion object{
        fun convertDateToShow(day: OneDayDate): String{
            var result: String = ""
            if(day.day < 10 || day.month < 10){
                var dayStr = "" + day.day
                var monthStr = "" + day.month
                when{
                    dayStr.toInt() < 10 -> {
                        dayStr = "0" + dayStr
                    }
                }
                when{
                    monthStr.toInt() < 10 -> {
                        monthStr = "0" + monthStr
                    }
                }
                result = "${dayStr}.${monthStr}.${day.year}"
            }else{
                result = "${day.day}.${day.month}.${day.year}"
            }
            return result
        }

        fun convertDateToShow(day: CalendarDay): String{
            var tempList: List<Int> = day.date.split(".").map { it.trim().toInt() }
            return convertDateToShow(
                OneDayDate(
                    tempList.get(0),
                    tempList.get(1),
                    tempList.get(2)
                )
            )
        }

        fun convertDateToDBString(day: OneDayDate): String{
            return "${day.day}.${day.month}.${day.year}"
        }

        fun convertDateToDB(date: String): String{
            var tempList: List<String> = date.split(".").map { it.trim() }
            return "${tempList.get(0).toInt()}.${tempList.get(1).toInt()}.${tempList.get(2).toInt()}"
        }

        fun convertTimeToShow(hour: Int, minute: Int): String{
            var result: String = ""
            if(hour < 10 || minute < 10){
                var hourStr = "" + hour
                var minuteStr = "" + minute
                when{
                    hourStr.toInt() < 10 -> {
                        hourStr = "0" + hourStr
                    }
                }
                when{
                    minuteStr.toInt() < 10 -> {
                        minuteStr = "0" + minuteStr
                    }
                }
                result = "$hourStr:$minuteStr"
            }else{
                result = "$hour:$minute"
            }
            return result
        }

        fun getDayFromDate(date: String): Int{
            var tempList: List<String> = date.split(".").map { it.trim() }
            return tempList.get(0).toInt()
        }

        fun getMonthFromDate(date: String): Int{
            var tempList: List<String> = date.split(".").map { it.trim() }
            return tempList.get(1).toInt()
        }

        fun getYearFromDate(date: String): Int {
            var tempList: List<String> = date.split(".").map { it.trim() }
            return tempList.get(2).toInt()
        }
    }
}