package com.example.calculate_pay_mvp.utils

import android.util.Log
import com.example.calculate_pay_mvp.app.AppPreferences
import java.util.*

class InitFormatDayUtils(
    var preferences: AppPreferences,
    var calendar: Calendar
) {

    private lateinit var rangeDays: LinkedList<Int>
    private lateinit var rangeMonth: LinkedList<Int>
    private lateinit var rangeYear: LinkedList<Int>

    fun initPaydayFormat(): Int{
        var format = preferences.getFromPreferences(AppPreferences.PREF_FORMAT_CHECK)
        when(format){
            AppPreferences.FORMAT_CHECK_ONEDAY ->{
                return initDaysOutOne()
            }
            AppPreferences.FORMAT_CHECK_TWODAY ->{
                return initDaysOutTwo()
            }
        }
        return -1
    }

    fun getPeriod(): List<List<Int>>{
        return listOf(rangeDays, rangeMonth, rangeYear)
    }

    fun getPeriodDays(): List<Int>{
        return rangeDays
    }

    fun getPeriodMonth(): List<Int>{
        return rangeMonth
    }

    fun getPeriodYear(): List<Int>{
        return rangeYear
    }

    private fun initDaysOutOne(): Int{
        var startPeriod: Int = preferences.getFromPreferences(AppPreferences.PREF_PAYDAY) + 1
        var endPeriod: Int = preferences.getFromPreferences(AppPreferences.PREF_PAYDAY)

        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()

        var curDay = calendar.get(Calendar.DAY_OF_MONTH)

        if(curDay > endPeriod){
            calendarStart.set(Calendar.DAY_OF_MONTH, startPeriod)
            calendarEnd.set(Calendar.MONTH, calendarEnd.get(Calendar.MONTH) + 1)
            calendarEnd.set(Calendar.DAY_OF_MONTH, endPeriod)
        }else{
            calendarStart.set(Calendar.MONTH, calendarStart.get(Calendar.MONTH) - 1)
            calendarStart.set(Calendar.DAY_OF_MONTH, startPeriod)
            calendarEnd.set(Calendar.DAY_OF_MONTH, endPeriod)
        }

        calendarEnd.set(Calendar.HOUR_OF_DAY, 0)
        calendarEnd.set(Calendar.MINUTE, 0)
        calendarEnd.set(Calendar.SECOND, 0)

        rangeDays = LinkedList()
        rangeDays.add(startPeriod)
        rangeDays.add(endPeriod)

        rangeMonth = LinkedList()
        rangeMonth.add(calendarStart.get(Calendar.MONTH))
        rangeMonth.add(calendarEnd.get(Calendar.MONTH))

        rangeYear = LinkedList()
        rangeYear.add(calendarStart.get(Calendar.YEAR))
        rangeYear.add(calendarEnd.get(Calendar.YEAR))

        var calendarTemp = Calendar.getInstance()

        var daysOut = ((calendarEnd.toInstant().toEpochMilli() - calendarTemp.toInstant().toEpochMilli())/86400000).toInt()

        return daysOut + 1
    }

    private fun initDaysOutTwo(): Int{
        var calendarCur = Calendar.getInstance()
        var nextPayday = getCloseDate(calendarCur.get(Calendar.DAY_OF_MONTH))
        preferences.putInPreferences(AppPreferences.PREF_PAYDAY, nextPayday)

        var startPeriod: Int = 1
        var endPeriod: Int = 15

        var calendarStart = Calendar.getInstance()
        var calendarEnd = Calendar.getInstance()
        var tempCalendar = Calendar.getInstance()

        if(preferences.getFromPreferences(AppPreferences.PREF_PAYDAY) == preferences.getFromPreferences(AppPreferences.PREF_PAYDAY_ONE)){
            tempCalendar.set(Calendar.MONTH, tempCalendar.get(Calendar.MONTH) - 1)
            startPeriod = 16
            if(startPeriod == 16){
                calendarStart.set(Calendar.MONTH, calendarStart.get(Calendar.MONTH) - 1)
                calendarEnd.set(Calendar.MONTH, calendarEnd.get(Calendar.MONTH) - 1)
                if(calendarCur.get(Calendar.DAY_OF_MONTH) > startPeriod){
                    calendarStart.set(Calendar.MONTH, calendarStart.get(Calendar.MONTH) + 1)
                    calendarEnd.set(Calendar.MONTH, calendarEnd.get(Calendar.MONTH) + 1)
                    tempCalendar.set(Calendar.MONTH, tempCalendar.get(Calendar.MONTH) + 1)
                }
            }
            endPeriod = tempCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        }

        calendarStart.set(Calendar.DAY_OF_MONTH, startPeriod)
        calendarEnd.set(Calendar.DAY_OF_MONTH, endPeriod)

        calendarEnd.set(Calendar.HOUR_OF_DAY, 0)
        calendarEnd.set(Calendar.MINUTE, 0)
        calendarEnd.set(Calendar.SECOND, 0)

        rangeDays = LinkedList()
        rangeDays.add(startPeriod)
        rangeDays.add(endPeriod)

        rangeMonth = LinkedList()
        rangeMonth.add(calendarStart.get(Calendar.MONTH))
        rangeMonth.add(calendarEnd.get(Calendar.MONTH))

        rangeYear = LinkedList()
        rangeYear.add(calendarStart.get(Calendar.YEAR))
        rangeYear.add(calendarEnd.get(Calendar.YEAR))

        var calendarTemp = Calendar.getInstance()
        var calendarPay = Calendar.getInstance()
        if(calendarTemp.get(Calendar.DAY_OF_MONTH) > nextPayday){
            calendarPay.set(Calendar.MONTH, calendarPay.get(Calendar.MONTH) + 1)
        }
        calendarPay.set(Calendar.DAY_OF_MONTH, nextPayday)
        calendarPay.set(Calendar.HOUR_OF_DAY, 0)
        calendarPay.set(Calendar.MINUTE, 0)
        calendarPay.set(Calendar.SECOND, 0)

        var daysOut = ((calendarPay.toInstant().toEpochMilli() - calendarTemp.toInstant().toEpochMilli())/86400000).toInt()

        Log.d("sss", "${rangeDays.get(0)}.${rangeMonth.get(0) + 1}.${rangeYear.get(0)} - ${rangeDays.get(1)}.${rangeMonth.get(1) + 1}.${rangeYear.get(1)}")

        return daysOut + 1
    }

    private fun getCloseDate(start: Int): Int{
        var onePayday: Int = preferences.getFromPreferences(AppPreferences.PREF_PAYDAY_ONE)
        var twoPayday: Int = preferences.getFromPreferences(AppPreferences.PREF_PAYDAY_TWO)

        //Log.d("myLogs", "$onePayday $twoPayday $start")

        var calendarOnePayday = Calendar.getInstance()
        var calendarTwoPayday = Calendar.getInstance()
        var curCalendar = Calendar.getInstance()

        var curDay = start

        if (curDay > onePayday){
            calendarOnePayday.set(Calendar.DAY_OF_MONTH, onePayday)
            calendarOnePayday.set(Calendar.MONTH, calendarOnePayday.get(Calendar.MONTH) + 1)
            curCalendar.set(Calendar.DAY_OF_MONTH, start)
        }else{
            calendarOnePayday.set(Calendar.DAY_OF_MONTH, onePayday)
            curCalendar.set(Calendar.DAY_OF_MONTH, start)
        }

        if (curDay > twoPayday){
            calendarTwoPayday.set(Calendar.DAY_OF_MONTH, twoPayday)
            calendarTwoPayday.set(Calendar.MONTH, calendarTwoPayday.get(Calendar.MONTH) + 1)
            curCalendar.set(Calendar.DAY_OF_MONTH, start)
        }else{
            calendarTwoPayday.set(Calendar.DAY_OF_MONTH, twoPayday)
            curCalendar.set(Calendar.DAY_OF_MONTH, start)
        }

        var timeOne = calendarOnePayday.toInstant().toEpochMilli()
        var timeTwo = calendarTwoPayday.toInstant().toEpochMilli()
        var timeCur = curCalendar.toInstant().toEpochMilli()


        Log.d("myLogs", "${timeOne - timeCur} ${timeTwo - timeCur}")

        if(timeOne - timeCur > timeTwo - timeCur)
            return calendarTwoPayday.get(Calendar.DAY_OF_MONTH)
        return calendarOnePayday.get(Calendar.DAY_OF_MONTH)
    }
}