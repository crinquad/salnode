package com.example.calculate_pay_mvp.callable

import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.database.CalendarDay
import java.util.concurrent.Callable

class GetTimeAllDB(var database: AppDatabase) : Callable<List<CalendarDay>>{
    override fun call(): List<CalendarDay> {
        return getTimeAllDB()
    }

    fun getTimeAllDB(): List<CalendarDay>{
        return database
            .calendarDayDao()
            .getTimeAll()
    }
}