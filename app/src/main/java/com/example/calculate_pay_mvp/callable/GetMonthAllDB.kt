package com.example.calculate_pay_mvp.callable

import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.database.CalendarDay
import java.util.concurrent.Callable

class GetMonthAllDB(var month: Int, var database: AppDatabase) : Callable<List<CalendarDay>> {
    override fun call(): List<CalendarDay> {
        return getMonthAllDB(month)
    }

    fun getMonthAllDB(month: Int): List<CalendarDay>{
        return database
            .calendarDayDao()
            .getMonthAll(month)
    }
}