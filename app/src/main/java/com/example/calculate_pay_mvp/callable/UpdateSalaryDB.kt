package com.example.calculate_pay_mvp.callable

import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.database.CalendarDay
import java.util.concurrent.Callable

class UpdateSalaryDB(var salaryPerHour: Float, var date: String, var database: AppDatabase) : Callable<CalendarDay> {

    private lateinit var mDatabase: AppDatabase

    override fun call(): CalendarDay {
        return updateSalaryDB(salaryPerHour, date)
    }

    fun updateSalaryDB(salaryPerHour: Float, date: String): CalendarDay {
        mDatabase = database

        var tempDay = mDatabase.calendarDayDao().findEntry(date)
        var tempCountHour = tempDay.countHour
        var newSalary = salaryPerHour * tempCountHour

        tempDay.salaryPerHour = salaryPerHour
        tempDay.salary = newSalary

        mDatabase.calendarDayDao().updateDay(tempDay)

        return tempDay
    }
}