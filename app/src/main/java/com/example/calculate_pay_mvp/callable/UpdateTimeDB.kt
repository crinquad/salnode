package com.example.calculate_pay_mvp.callable

import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.unit.WorkHours
import java.util.concurrent.Callable

class UpdateTimeDB (var timeTo: String, var date: String, var toOrFromCode: Int, var database: AppDatabase) :
    Callable<CalendarDay> {

    private lateinit var mDatabase: AppDatabase
    private lateinit var mListFrom: List<Int>
    private lateinit var mListTo: List<Int>

    companion object{
        val LEFT_TIME = 0
        val RIGHT_TIME = 1
    }

    override fun call(): CalendarDay {
        if(toOrFromCode == LEFT_TIME) {
            return updateFromTimeDB(timeTo, date)
        }
        return updateToTimeDB(timeTo, date)
    }

    fun updateToTimeDB(timeTo: String, date: String): CalendarDay {
        mDatabase = database

        var tempDay = mDatabase.calendarDayDao().findEntry(date)
        tempDay.timeTo = timeTo

        mListFrom = tempDay.timeFrom.split(":").map { it.trim().toInt() }
        mListTo = tempDay.timeTo.split(":").map { it.trim().toInt() }

        var workHours = WorkHours(
            mListFrom.get(0),
            mListFrom.get(1),
            mListTo.get(0),
            mListTo.get(1)
        )

        tempDay.countHour = workHours.calculateTime()
        tempDay.salary = tempDay.salaryPerHour * tempDay.countHour

        mDatabase.calendarDayDao().updateDay(tempDay)
        return tempDay
    }

    fun updateFromTimeDB(timeFrom: String, date: String): CalendarDay {
        mDatabase = database

        var tempDay = mDatabase.calendarDayDao().findEntry(date)
        tempDay.timeFrom = timeFrom

        mListFrom = tempDay.timeFrom.split(":").map { it.trim().toInt() }
        mListTo = tempDay.timeTo.split(":").map { it.trim().toInt() }

        var workHours = WorkHours(
            mListFrom.get(0),
            mListFrom.get(1),
            mListTo.get(0),
            mListTo.get(1)
        )

        tempDay.countHour = workHours.calculateTime()
        tempDay.salary = tempDay.salaryPerHour * tempDay.countHour

        mDatabase.calendarDayDao().updateDay(tempDay)
        return tempDay
    }
}