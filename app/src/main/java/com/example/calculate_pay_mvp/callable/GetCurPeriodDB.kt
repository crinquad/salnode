package com.example.calculate_pay.callable

import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.database.CalendarDay
import java.util.*
import java.util.concurrent.Callable

class GetCurPeriodDB(
    var period: List<Int>,
    var periodMonth: List<Int>,
    var periodYear: List<Int>,
    var database: AppDatabase
) : Callable<List<CalendarDay>> {
    override fun call(): List<CalendarDay> {
        return getCurPeriodDB(
            period.get(0),
            period.get(1),
            periodMonth.get(0) + 1,
            periodMonth.get(1) + 1,
            periodYear.get(0),
            periodYear.get(1),
            database
        )
    }

    fun getCurPeriodDB(
        startPeriod: Int,
        endPeriod: Int,
        monthStart: Int,
        monthEnd: Int,
        startYear: Int,
        endYear: Int,
        database: AppDatabase
    ): List<CalendarDay> {
        if (monthStart == monthEnd) {
            var monthLeft = database
                .calendarDayDao()
                .getPeriodStat(startPeriod, endPeriod, monthStart, startYear)
            return monthLeft
        }
        var calendar: Calendar = Calendar.getInstance()
        var monthLeft = database
            .calendarDayDao()
            .getPeriodStat(
                startPeriod,
                calendar.getActualMaximum(Calendar.DAY_OF_MONTH),
                monthStart,
                startYear
            )

        calendar.set(Calendar.MONTH, monthEnd)

        var monthRight = database
            .calendarDayDao()
            .getPeriodStat(
                calendar.getActualMinimum(Calendar.DAY_OF_MONTH),
                endPeriod,
                monthEnd,
                endYear
            )

        var result: LinkedList<CalendarDay> = LinkedList()
        result.addAll(monthLeft)
        result.addAll(monthRight)
        return result
    }

}