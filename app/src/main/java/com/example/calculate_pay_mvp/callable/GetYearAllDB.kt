package com.example.calculate_pay_mvp.callable

import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.database.CalendarDay
import java.util.concurrent.Callable

class GetYearAllDB(var year: Int, var database: AppDatabase) : Callable<List<CalendarDay>> {
    override fun call(): List<CalendarDay> {
        return getYearAllDB(year)
    }

    fun getYearAllDB(year: Int): List<CalendarDay>{
        return database
            .calendarDayDao()
            .getYearAll(year)
    }
}