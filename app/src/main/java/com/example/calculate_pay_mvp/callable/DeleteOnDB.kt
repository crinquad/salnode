package com.example.calculate_pay.callable

import com.example.calculate_pay.database.AppDatabase
import java.util.concurrent.Callable

class DeleteOnDB(var date: String, var database: AppDatabase) : Callable<Int> {
    override fun call(): Int {
        return deleteOnDB(date, database)
    }

    fun deleteOnDB(date: String, database: AppDatabase): Int{
        return database
            .calendarDayDao()
            .deleteDayDate(date)
    }
}