package com.example.calculate_pay.callable

import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.database.CalendarDay
import java.util.concurrent.Callable

class FindEntryDB(var date: String, var database: AppDatabase) : Callable<CalendarDay> {
    override fun call(): CalendarDay {
        return findEntryDB(date, database)
    }

    fun findEntryDB(date: String, database:AppDatabase) : CalendarDay {
        return database
            .calendarDayDao()
            .findEntry(date)
    }
}