package com.example.calculate_pay_mvp.callable

import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.unit.OneDayDate
import java.util.concurrent.Callable

class CheckExistDB(var day: OneDayDate, var database: AppDatabase) : Callable<Boolean> {

    override fun call(): Boolean {
        return checkExist(day, database)
}

    fun checkExist(day: OneDayDate, database: AppDatabase): Boolean{
        var flag = database
            .calendarDayDao()
            .checkExist("${day.day}.${day.month}.${day.year}")
        return flag

    }
}