package com.example.calculate_pay_mvp.callable

import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay.database.CalendarDay
import java.util.concurrent.Callable

class InsertInDB(var day: CalendarDay, var database: AppDatabase) : Callable<CalendarDay> {

    override fun call(): CalendarDay {
        return insertInDb(day, database)
    }

    private fun insertInDb(day: CalendarDay, database: AppDatabase): CalendarDay {
            database
            .calendarDayDao()
            .insertDay(day)
        return day
    }
}