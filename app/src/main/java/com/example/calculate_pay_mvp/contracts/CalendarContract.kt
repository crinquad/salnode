package com.example.calculate_pay_mvp.contracts

import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.base.MvpPresenter
import com.example.calculate_pay_mvp.base.MvpView

interface CalendarContract {

    interface View : MvpView{
        fun initAdapter(date: Array<OneDayDate>)
        fun showTextDate(month: Int, year: Int)
        fun showDialog(day: OneDayDate)
        fun showExistDay(day: OneDayDate)
    }

    interface Presenter : MvpPresenter<View>{
        fun initDate()
        fun refreshDate(code: Int)
        fun saveDay(day: CalendarDay)
        fun showDay(day: OneDayDate)
    }
}