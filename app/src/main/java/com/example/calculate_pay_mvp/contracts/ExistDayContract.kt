package com.example.calculate_pay_mvp.contracts

import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.base.MvpPresenter
import com.example.calculate_pay_mvp.base.MvpView

interface ExistDayContract {

    interface View : MvpView{
        fun initTextFromEntry(day: CalendarDay)
        fun showTimeDialog()
        fun showSalaryDialog()
        fun getDateText(): String
        fun destroy()
    }

    interface Presenter: MvpPresenter<View>{
        fun initDay(day: OneDayDate)
        fun changeTimeFrom()
        fun changeTimeTo()
        fun changeSalaryHour()
        fun deleteDay()
        fun updateSalaryHour(newSalary: Float)
        fun updateTime(time: String)
    }
}