package com.example.calculate_pay_mvp.contracts

import com.example.calculate_pay_mvp.base.MvpPresenter
import com.example.calculate_pay_mvp.base.MvpView

interface MainContract{

    interface View : MvpView {
        fun showDaysOut(daysOut: Int)
        fun showDaysCount(daysCount: Int)
        fun showHourCount(hourCount: Float)
        fun showSalaryGlobal(salaryGlobal: Float)
    }

    interface Presenter :
        MvpPresenter<View> {
        fun loadDaysOut()
        fun loadWorkData()
    }
}