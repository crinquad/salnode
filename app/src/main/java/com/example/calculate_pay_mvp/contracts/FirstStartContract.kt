package com.example.calculate_pay_mvp.contracts

import com.example.calculate_pay_mvp.base.MvpPresenter
import com.example.calculate_pay_mvp.base.MvpView

interface FirstStartContract {

    interface View : MvpView {
        fun getInputFormOne(): String
        fun getInputFormTwo(): Array<String>
    }

    interface Presenter:
        MvpPresenter<View> {
        fun putReg(): Boolean
        fun uncheckFirstStart()
        fun checkValidOne(): Boolean
        fun checkValidTwo(): Boolean
    }
}