package com.example.calculate_pay_mvp.contracts

import com.example.calculate_pay_mvp.base.MvpPresenter
import com.example.calculate_pay_mvp.base.MvpView

interface StatisticPageContract{

    interface View : MvpView{
        fun initPage()
        fun showDay(hours: Float, salary: Float, days: Int)
    }

    interface Presenter : MvpPresenter<View>{
        fun initMonth()
        fun initYear()
        fun initAllTime()
    }
}