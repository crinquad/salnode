package com.example.calculate_pay.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.R
import com.example.calculate_pay_mvp.callable.CheckExistDB
import com.example.calculate_pay_mvp.database.DBHelper
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import java.lang.ClassCastException
import java.util.*

class CalendarAdapter(var ctx: Context, var days: Array<OneDayDate>, var dbHelper: DBHelper) : BaseAdapter(), View.OnClickListener {

    interface OnCalendarItemListener {
        fun onItemClick(day: OneDayDate)
    }

    lateinit var calendarItemListener: OnCalendarItemListener
    lateinit var existsDays: LinkedList<OneDayDate>
    var inflater: LayoutInflater

    init {
        inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        existsDays = LinkedList()
        try {
            calendarItemListener = ctx as OnCalendarItemListener
        }catch (e: ClassCastException){
            //...
        }
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        var view = convertView as TextView?
        var day: OneDayDate = getDay(position)
        if (view == null) {
            view = inflater.inflate(R.layout.item_day, parent, false) as TextView
        }

        checkExistAndSwap(view, day)

        if (view != null) {
            view.setText(day.day.toString())
            view.setTag(position)
            view.setOnClickListener(this)
        }

        return view
    }

    private fun swapItemType(view: TextView) {
        view.setBackgroundResource(R.drawable.style_item_day_saved)
        view.setTextColor(ctx.resources.getColor(R.color.gray))
    }

    private fun checkExistAndSwap(view: TextView, day: OneDayDate) {
        dbHelper.checkExist(day)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<Boolean>{
                    override fun accept(t: Boolean?) {
                        if(t == true){
                            swapItemType(view)
                            existsDays.add(day)
                        }
                    }

                }
            )
    }

    fun checkExistsDay(day: OneDayDate): Boolean{
        for(i in 0..existsDays.size-1){
            if(day.hashCode() == existsDays.get(i).hashCode()){
                return true
            }
        }
        return false
    }

    override fun getItem(position: Int): Any {
        return days.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return days.size
    }

    fun getDay(position: Int): OneDayDate {
        return days.get(position)
    }

    override fun onClick(v: View) {
        var temp: OneDayDate = getDay(v.getTag() as Int)
        calendarItemListener.onItemClick(temp)
    }

}