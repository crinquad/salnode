package com.example.calculate_pay_mvp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.calculate_pay.fragments.StatisticPageFragment
import com.example.calculate_pay_mvp.fragment.StatisticAllFragment.Companion.PAGE_COUNT

class StatisticPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

    override fun getItemCount(): Int {
        return PAGE_COUNT
    }

    override fun createFragment(position: Int): Fragment {
        return StatisticPageFragment.newInstance(position)
    }


}