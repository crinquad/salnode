package com.example.calculate_pay.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.calculate_pay_mvp.R


class SpinnerAdapter : ArrayAdapter<String>{

    var objects: Array<String>

    constructor(context: Context, textViewResourceId: Int, objects : Array<String>) : super(context, textViewResourceId, objects){
        this.objects = objects
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    fun getCustomView(position: Int, convertView: View?, parent: ViewGroup): View{
        var inflater = LayoutInflater.from(parent.context)
        var row = inflater.inflate(R.layout.spinner_layout, parent, false)
        var label: TextView = row.findViewById(R.id.spinnerGr)
        label.setText(objects[position])

        return row
    }
}