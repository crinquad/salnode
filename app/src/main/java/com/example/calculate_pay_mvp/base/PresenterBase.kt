package com.example.calculate_pay_mvp.base

abstract class PresenterBase<T : MvpView> :
    MvpPresenter<T> {

    private var view: T? = null

    override fun attachView(mvpView: T) {
        view = mvpView
    }

    override fun detachView() {
        view = null
    }

    fun getView(): T?{
        return view
    }

    protected fun isViewAttached(): Boolean{
        return view != null
    }
}