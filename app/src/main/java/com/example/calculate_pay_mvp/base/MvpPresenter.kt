package com.example.calculate_pay_mvp.base

interface MvpPresenter<T : MvpView> {

    fun attachView(mvpView: T)

    fun viewIsReady()

    fun detachView()

}