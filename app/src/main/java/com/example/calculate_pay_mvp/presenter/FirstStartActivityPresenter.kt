package com.example.calculate_pay_mvp.presenter

import com.example.calculate_pay_mvp.app.AppPreferences
import com.example.calculate_pay_mvp.base.PresenterBase
import com.example.calculate_pay_mvp.contracts.FirstStartContract

class FirstStartActivityPresenter(val preferences: AppPreferences) :
    PresenterBase<FirstStartContract.View>(), FirstStartContract.Presenter {

    var view: FirstStartContract.View? = null

    override fun attachView(mvpView: FirstStartContract.View) {
        view = mvpView
    }

    override fun viewIsReady() {
        //...
    }

    override fun detachView() {
        view = null
    }

    override fun putReg(): Boolean {
        if (checkValidOne()) {
            preferences.putInPreferences(
                AppPreferences.PREF_FORMAT_CHECK,
                AppPreferences.FORMAT_CHECK_ONEDAY
            )
            return true
        }
        if (checkValidTwo()) {
            preferences.putInPreferences(
                AppPreferences.PREF_FORMAT_CHECK,
                AppPreferences.FORMAT_CHECK_TWODAY
            )
            return true
        }
        return false
    }

    override fun uncheckFirstStart() {
        preferences.putInPreferences(
            AppPreferences.PREF_FIRST_CHECK,
            AppPreferences.FIRST_CHECK_FALSE
        )
    }

    override fun checkValidOne(): Boolean {
        var day = view?.getInputFormOne()
        if (!day.equals("")) {
            if (day?.toInt()!! > 0 && day?.toInt() < 29) {
                preferences.putInPreferences(AppPreferences.PREF_PAYDAY, day.toInt())
                uncheckFirstStart()
                return true
            }
        }
        return false
    }

    override fun checkValidTwo(): Boolean {
        var days = view?.getInputFormTwo()
        var day1 = days?.get(0)
        var day2 = days?.get(1)
        if (!day1.equals("") && !day2.equals("")) {
            if (day1?.toInt()!! > 0 && day1?.toInt() < 29) {
                if (day2?.toInt()!! > 0 && day2?.toInt() < 29) {
                    preferences.putInPreferences(AppPreferences.PREF_PAYDAY_ONE, day1.toInt())
                    preferences.putInPreferences(AppPreferences.PREF_PAYDAY_TWO, day2.toInt())
                    uncheckFirstStart()
                    return true
                }
            }
        }
        return false
    }
}