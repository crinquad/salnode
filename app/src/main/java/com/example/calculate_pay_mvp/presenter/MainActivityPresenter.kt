package com.example.calculate_pay_mvp.presenter

import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay_mvp.base.PresenterBase
import com.example.calculate_pay_mvp.database.DBHelper
import com.example.calculate_pay_mvp.contracts.MainContract
import com.example.calculate_pay_mvp.utils.InitFormatDayUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers


class MainActivityPresenter(
    val initFormatDayUtils: InitFormatDayUtils,
    val dbHelper: DBHelper
) : PresenterBase<MainContract.View>(), MainContract.Presenter {

    private var view: MainContract.View? = null

    override fun attachView(mvpView: MainContract.View) {
        view = mvpView
    }

    override fun viewIsReady() {
        loadDaysOut()
        loadWorkData()
    }

    override fun detachView() {
        view = null
    }

    override fun loadDaysOut(){
        view?.showDaysOut(initFormatDayUtils.initPaydayFormat())
    }

    override fun loadWorkData(){
        var globalHours = 0F
        var globalDays = 0
        var globalSalary = 0F
        dbHelper.getCurrentPeriod(
            initFormatDayUtils.getPeriodDays(),
            initFormatDayUtils.getPeriodMonth(),
            initFormatDayUtils.getPeriodYear()
        )
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<List<CalendarDay>>{
                    override fun accept(t: List<CalendarDay>) {
                        for(i in 0..t.size-1){
                            globalHours += t.get(i).countHour
                            globalSalary += t.get(i).salary
                            globalDays++
                        }
                        view?.showHourCount(globalHours)
                        view?.showSalaryGlobal(globalSalary)
                        view?.showDaysCount(globalDays)
                    }

                }
            )
    }
}