package com.example.calculate_pay_mvp.presenter

import android.util.Log
import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.base.PresenterBase
import com.example.calculate_pay_mvp.callable.UpdateTimeDB
import com.example.calculate_pay_mvp.contracts.ExistDayContract
import com.example.calculate_pay_mvp.database.DBHelper
import com.example.calculate_pay_mvp.utils.DateConvertUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class ExistDayActivityPresenter(val dbHelper: DBHelper, var day: OneDayDate) :
    PresenterBase<ExistDayContract.View>(), ExistDayContract.Presenter {

    var view: ExistDayContract.View? = null
    var flagTimeFrom = true

    override fun attachView(mvpView: ExistDayContract.View) {
        view = mvpView
    }

    override fun viewIsReady() {
        initDay(day)
    }

    override fun detachView() {
        view = null
    }

    override fun initDay(day: OneDayDate) {
        dbHelper.findEntryDB(DateConvertUtils.convertDateToDBString(day))
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<CalendarDay> {
                    override fun accept(t: CalendarDay) {
                        view?.initTextFromEntry(t)
                    }
                }
            )
    }

    override fun changeTimeFrom() {
        flagTimeFrom = true
        view?.showTimeDialog()
    }

    override fun changeTimeTo() {
        flagTimeFrom = false
        view?.showTimeDialog()
    }

    override fun changeSalaryHour() {
        view?.showSalaryDialog()
    }

    override fun deleteDay() {
        var date = DateConvertUtils.convertDateToDB(view?.getDateText()!!)
        dbHelper.deleteRow(date)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<Int> {
                    override fun accept(t: Int?) {
                        view?.destroy()
                    }
                }
            )
    }

    override fun updateSalaryHour(newSalary: Float) {
        var date = DateConvertUtils.convertDateToDB(view?.getDateText()!!)
        dbHelper.updateSalaryPerHour(newSalary, date)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<CalendarDay> {
                    override fun accept(t: CalendarDay) {
                        view?.initTextFromEntry(t)
                    }

                }
            )
    }

    override fun updateTime(time: String) {
        var date = DateConvertUtils.convertDateToDB(view?.getDateText()!!)
        when (flagTimeFrom) {
            true -> {
                if (date != null) {
                    dbHelper.updateTime(time, date, UpdateTimeDB.LEFT_TIME)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            object : Consumer<CalendarDay> {
                                override fun accept(t: CalendarDay) {
                                    view?.initTextFromEntry(t)
                                }

                            }
                        )
                }
            }
            false -> {
                if (date != null) {
                    dbHelper.updateTime(time, date, UpdateTimeDB.RIGHT_TIME)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            object : Consumer<CalendarDay> {
                                override fun accept(t: CalendarDay) {
                                    view?.initTextFromEntry(t)
                                }

                            }
                        )
                }
            }
        }
    }


}