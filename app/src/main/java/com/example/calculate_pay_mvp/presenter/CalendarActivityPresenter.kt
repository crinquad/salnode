package com.example.calculate_pay_mvp.presenter

import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.datemanagers.DateManager
import com.example.calculate_pay.datemanagers.RealDateManager
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.base.PresenterBase
import com.example.calculate_pay_mvp.contracts.CalendarContract
import com.example.calculate_pay_mvp.database.DBHelper
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class CalendarActivityPresenter(
    var dateManager: DateManager,
    var realDateManager: RealDateManager,
    var dbHelper: DBHelper
) : PresenterBase<CalendarContract.View>(), CalendarContract.Presenter {

    var view: CalendarContract.View? = null
    lateinit var date: Array<OneDayDate>
    lateinit var dateLeft: Array<OneDayDate>

    override fun attachView(mvpView: CalendarContract.View) {
        view = mvpView
    }

    override fun viewIsReady() {
        initDate()
    }

    override fun detachView() {
        view = null
    }

    override fun initDate() {
        date = dateManager.getArrayMonthCalendar(realDateManager.getMonth(), realDateManager.getYear())
        dateLeft = dateManager.getMonthLeft()
        view?.initAdapter(date)
        view?.showTextDate(realDateManager.getMonth(), realDateManager.getYear())
    }

    override fun refreshDate(code: Int) {
        when(code){
            RealDateManager.MONTH_DECREMENT -> realDateManager.refreshDate(RealDateManager.MONTH_DECREMENT)
            RealDateManager.MONTH_INCREMENT -> realDateManager.refreshDate(RealDateManager.MONTH_INCREMENT)
            RealDateManager.MONTH_REFRESH -> realDateManager.refreshDate(RealDateManager.MONTH_REFRESH)
        }
        initDate()
    }

    override fun saveDay(day: CalendarDay) {
        dbHelper.insertRow(day)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<CalendarDay>{
                    override fun accept(t: CalendarDay?) {
                        refreshDate(RealDateManager.MONTH_REFRESH)
                    }

                }
            )
    }

    override fun showDay(day: OneDayDate) {
        dbHelper.checkExist(day)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<Boolean>{
                    override fun accept(t: Boolean?) {
                        if(t == true){
                            view?.showExistDay(day)
                        }
                        else{
                            view?.showDialog(day)
                        }
                    }
                }
            )
    }
}