package com.example.calculate_pay_mvp.presenter

import com.example.calculate_pay.database.CalendarDay
import com.example.calculate_pay.datemanagers.RealDateManager
import com.example.calculate_pay_mvp.base.MvpView
import com.example.calculate_pay_mvp.base.PresenterBase
import com.example.calculate_pay_mvp.contracts.StatisticPageContract
import com.example.calculate_pay_mvp.database.DBHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers

class StatisticPagePresenter(var dbHelper: DBHelper, var realDateManager: RealDateManager) :
    PresenterBase<StatisticPageContract.View>(), StatisticPageContract.Presenter {

    var view: StatisticPageContract.View? = null

    override fun attachView(mvpView: StatisticPageContract.View) {
        view = mvpView
    }

    override fun viewIsReady() {
        view?.initPage()
    }

    override fun detachView() {
        view = null
    }

    override fun initMonth() {
        var globalHours = 0F
        var globalDays = 0
        var globalSalary = 0F
        dbHelper.getRowsMonth(realDateManager.getMonth() + 1)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<List<CalendarDay>> {
                    override fun accept(t: List<CalendarDay>) {
                        for (i in 0..t.size - 1) {
                            globalHours += t.get(i).countHour
                            globalSalary += t.get(i).salary
                            globalDays++
                        }
                        view?.showDay(globalHours, globalSalary, globalDays)
                    }

                }
            )
    }

    override fun initYear() {
        var globalHours = 0F
        var globalDays = 0
        var globalSalary = 0F
        dbHelper.getRowsYear(realDateManager.getYear())
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<List<CalendarDay>> {
                    override fun accept(t: List<CalendarDay>) {
                        for (i in 0..t.size - 1) {
                            globalHours += t.get(i).countHour
                            globalSalary += t.get(i).salary
                            globalDays++
                        }
                        view?.showDay(globalHours, globalSalary, globalDays)
                    }

                }
            )
    }

    override fun initAllTime() {
        var globalHours = 0F
        var globalDays = 0
        var globalSalary = 0F
        dbHelper.getAllRows()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                object : Consumer<List<CalendarDay>> {
                    override fun accept(t: List<CalendarDay>) {
                        for (i in 0..t.size - 1) {
                            globalHours += t.get(i).countHour
                            globalSalary += t.get(i).salary
                            globalDays++
                        }
                        view?.showDay(globalHours, globalSalary, globalDays)
                    }

                }
            )
    }
}