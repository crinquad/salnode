package com.example.calculate_pay.unit

class WorkHours {

    private var mHourFrom:Int
    private var mMinuteFrom:Int
    private var mHourTo:Int
    private var mMinuteTo:Int

    constructor(mHourFrom: Int, mMinuteFrom: Int, mHourTo: Int, mMinuteTo: Int) {
        this.mHourFrom = mHourFrom
        this.mMinuteFrom = mMinuteFrom
        this.mHourTo = mHourTo
        this.mMinuteTo = mMinuteTo
    }

    fun calculateTime(): Float{
        var hours = 0
        if (mHourTo < mHourFrom){
            hours = (mHourTo + 24) - mHourFrom
        }else{
            hours = mHourTo - mHourFrom
        }

        var minutes = 0
        if(mMinuteTo < mMinuteFrom){
            minutes = (mMinuteTo + 60) - mMinuteFrom
            hours -= 1
        }else{
            minutes = mMinuteTo - mMinuteFrom
        }

        var result: Float = hours.toFloat() + (minutes.toFloat()/60)

        return result
    }

    fun getHourFrom(): Int{
        return mHourFrom
    }

    fun getHourTo(): Int{
        return mHourTo
    }

    fun getMinuteFrom(): Int{
        return mMinuteFrom
    }

    fun getMinuteTo(): Int{
        return mMinuteTo
    }
}