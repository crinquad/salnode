package com.example.calculate_pay_mvp.component

import com.example.calculate_pay_mvp.presenter.MainActivityPresenter
import com.example.calculate_pay_mvp.module.MainModule
import dagger.Subcomponent

@Subcomponent(modules = [MainModule::class])
interface MainComponent {
    fun getMainActivityPresenter(): MainActivityPresenter
}