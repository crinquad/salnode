package com.example.calculate_pay_mvp.component

import com.example.calculate_pay.adapter.CalendarAdapter
import com.example.calculate_pay_mvp.module.CalendarAdapterModule
import dagger.Subcomponent

@Subcomponent(modules = [CalendarAdapterModule::class])
interface AdapterComponent {
    fun getCalendarAdapter(): CalendarAdapter
}