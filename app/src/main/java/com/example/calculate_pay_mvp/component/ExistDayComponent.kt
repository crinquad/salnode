package com.example.calculate_pay_mvp.component

import com.example.calculate_pay_mvp.module.ExistDayModule
import com.example.calculate_pay_mvp.presenter.ExistDayActivityPresenter
import dagger.Subcomponent

@Subcomponent(modules = [ExistDayModule::class])
interface ExistDayComponent {
    fun getExistDayActivityPresenter(): ExistDayActivityPresenter
}