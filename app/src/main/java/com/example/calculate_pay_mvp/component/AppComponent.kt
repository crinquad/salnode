package com.example.calculate_pay_mvp.component

import android.content.Context
import android.os.Bundle
import com.example.calculate_pay.datemanagers.DateManager
import com.example.calculate_pay.datemanagers.RealDateManager
import com.example.calculate_pay_mvp.app.AppPreferences
import com.example.calculate_pay_mvp.app.AppScope
import com.example.calculate_pay_mvp.database.DBHelper
import com.example.calculate_pay_mvp.module.*
import dagger.BindsInstance
import dagger.Component
import java.util.*

@AppScope
@Component(modules = [AppModule::class, StorageModule::class, DateManageModule::class])
interface AppComponent {

    fun getPreferences(): AppPreferences
    fun getBundle() : Bundle
    fun getDBHelper(): DBHelper
    fun getMainComponent(): MainComponent
    fun getFirstStartComponent(): FirstStartComponent
    fun getCalendarComponent(): CalendarComponent
    fun getAdapterComponent(calendarAdapterModule: CalendarAdapterModule): AdapterComponent
    fun getDayDialogComponent(dayDialogModule: DayDialogModule): DayDialogComponent
    fun getExistDayComponent(existDayModule: ExistDayModule): ExistDayComponent
    fun getDialogSalaryComponent() : DialogSalaryComponent
    fun getStatisticPageComponent() : StatisticPageComponent
    fun getCalendar(): Calendar
    fun getDateManager(): DateManager
    fun getRealDateManager(): RealDateManager

    @Component.Builder
    interface Builder{
        fun getAppComp(): AppComponent
        fun storageModule(storageModule: StorageModule): Builder
        @BindsInstance
        fun context(context: Context): Builder
    }
}