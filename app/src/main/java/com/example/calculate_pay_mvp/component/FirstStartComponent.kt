package com.example.calculate_pay_mvp.component

import com.example.calculate_pay_mvp.presenter.FirstStartActivityPresenter
import com.example.calculate_pay_mvp.module.FirstStartModule
import dagger.Subcomponent

@Subcomponent(modules = [FirstStartModule::class])
interface FirstStartComponent {
    fun getFirstStartActivityPresenter() : FirstStartActivityPresenter
}