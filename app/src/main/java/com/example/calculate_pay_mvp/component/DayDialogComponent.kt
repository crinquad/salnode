package com.example.calculate_pay_mvp.component

import com.example.calculate_pay_mvp.fragment.DayDialogFragment
import com.example.calculate_pay_mvp.module.DayDialogModule
import dagger.Subcomponent

@Subcomponent(modules = [DayDialogModule::class])
interface DayDialogComponent {
    fun getDayDialog(): DayDialogFragment
}