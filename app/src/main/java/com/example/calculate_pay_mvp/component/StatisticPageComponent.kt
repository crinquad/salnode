package com.example.calculate_pay_mvp.component

import com.example.calculate_pay_mvp.module.StatisticPageModule
import com.example.calculate_pay_mvp.presenter.StatisticPagePresenter
import dagger.Subcomponent

@Subcomponent(modules = [StatisticPageModule::class])
interface StatisticPageComponent {
    fun getStatisticPagePresenter(): StatisticPagePresenter
}