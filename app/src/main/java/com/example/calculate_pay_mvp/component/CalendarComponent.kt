package com.example.calculate_pay_mvp.component

import android.content.Context
import com.example.calculate_pay.adapter.CalendarAdapter
import com.example.calculate_pay.unit.OneDayDate
import com.example.calculate_pay_mvp.module.CalendarAdapterModule
import com.example.calculate_pay_mvp.module.CalendarModule
import com.example.calculate_pay_mvp.presenter.CalendarActivityPresenter
import dagger.BindsInstance
import dagger.Subcomponent

@Subcomponent(modules = [CalendarModule::class])
interface CalendarComponent {
    fun getCalendarActivityPresenter(): CalendarActivityPresenter
}