package com.example.calculate_pay_mvp.component

import com.example.calculate_pay_mvp.fragment.DialogSalaryFragment
import com.example.calculate_pay_mvp.module.DialogSalaryModule
import dagger.Lazy
import dagger.Subcomponent

@Subcomponent(modules = [DialogSalaryModule::class])
interface DialogSalaryComponent {
    fun getSalaryDialog() : Lazy<DialogSalaryFragment>
}