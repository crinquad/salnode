package com.example.calculate_pay_mvp.app

import android.content.SharedPreferences

class AppPreferences(private var preferences: SharedPreferences) {

    companion object{
        val PREF_NAME_FORMAT = "date_format"

        val PREF_FIRST_CHECK = "first_start"
        val FIRST_CHECK_TRUE = 0
        val FIRST_CHECK_FALSE = 1

        val PREF_FORMAT_CHECK = "format_check"
        val FORMAT_CHECK_ONEDAY = 0
        val FORMAT_CHECK_TWODAY = 1

        val PREF_PAYDAY = "pref_payday_cur"
        val PREF_PAYDAY_ONE = "pref_payday_one"
        val PREF_PAYDAY_TWO = "pref_payday_two"
    }

    fun putInPreferences(name: String, signal: Int) {
        preferences
            .edit()
            .putInt(name, signal)
            .commit()
    }

    fun checkPref(name: String) : Int{
        return preferences.getInt(name, 0)
    }

    fun getFromPreferences(name: String): Int{
        return preferences.getInt(name, 0)
    }

}