package com.example.calculate_pay_mvp.app

import com.example.calculate_pay_mvp.R

class Arrays {

    companion object{
        val month:Array<Int> = arrayOf(
            R.string.january,
            R.string.february,
            R.string.march,
            R.string.april,
            R.string.may,
            R.string.june,
            R.string.july,
            R.string.august,
            R.string.september,
            R.string.oktober,
            R.string.november,
            R.string.december
        )
    }
}