package com.example.calculate_pay_mvp.app

import android.app.Application
import com.example.calculate_pay.database.AppDatabase
import com.example.calculate_pay_mvp.component.AppComponent
import com.example.calculate_pay_mvp.component.DaggerAppComponent
import com.example.calculate_pay_mvp.module.StorageModule

class App : Application() {

    lateinit var appComponent: AppComponent

    init {
        instance = this
    }

    companion object {
        private var instance: App? = null

        fun applicationContext() : App {
            return instance as App
        }
    }

    override fun onCreate() {
        super.onCreate()
        appComponent= DaggerAppComponent.builder()
            .context(this)
            .storageModule(
                StorageModule(
                    AppDatabase::class.java
                )
            )
            .getAppComp()

    }

}